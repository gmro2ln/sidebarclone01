import { createRouter, createWebHistory } from 'vue-router'
import HelloComponent from '@/components/HelloComponent.vue'
import NumberList from '@/components/NumberList.vue'
import StaticHtml from '@/components/StaticHtml.vue';
import AddMoreHTML from '@/components/AddMoreHTML.vue';
import PULthi1wBmf from '@/components/PULthi1wBmf.vue';
import Sun91024923Close from '@/components/Sun91024923Close.vue';
import Sun81101597Close from '@/components/Sun81101597Close.vue';
import Sun46134717Close from '@/components/Sun46134717Close.vue';
import Sun5532663Close from '@/components/Sun5532663Close.vue';
import Sun9446934Close from '@/components/Sun9446934Close.vue';
import Sun10808851Close from '@/components/Sun10808851Close.vue';
import Sun90109750Close from '@/components/Sun90109750Close.vue';
import Sun76147235Close from '@/components/Sun76147235Close.vue';
import outBoxPrompt from '@/components/outBoxPrompt.vue';
import iconsCopy from '@/components/iconsCopy.vue';
import promptGuitarist from '@/components/promptGuitarist.vue';
import EminorScaleHelper from '@/components/EminorScaleHelper.vue';
//place1

const routes = [
  {
    path: '/hello',
    name: 'Hello',
    component: HelloComponent
  },
  {
    path: '/number-list',
    name: 'NumberList',
    component: NumberList
  },
  { path: '/FSh@kNX8vQ23.html', name: 'StaticHtml', component: StaticHtml },
  { path: '/AddMoreHTML.html', name: 'AddMoreHTML', component: AddMoreHTML },
  { path: '/PULthi1wBmf.html', name: 'PULthi1wBmf', component: PULthi1wBmf },
  { path: '/Sun91024923Close.html', name: 'Sun91024923Close', component: Sun91024923Close },
  { path: '/Sun81101597Close.html', name: 'Sun81101597Close', component: Sun81101597Close },
  { path: '/Sun46134717Close.html', name: 'Sun46134717Close', component: Sun46134717Close },
  { path: '/Sun5532663Close.html', name: 'Sun5532663Close', component: Sun5532663Close },
  { path: '/Sun9446934Close.html', name: 'Sun9446934Close', component: Sun9446934Close },
  { path: '/Sun10808851Close.html', name: 'Sun10808851Close', component: Sun10808851Close },
  { path: '/Sun90109750Close.html', name: 'Sun90109750Close', component: Sun90109750Close },
  { path: '/Sun76147235Close.html', name: 'Sun76147235Close', component: Sun76147235Close },
  { path: '/outBoxPrompt.html', name: 'outBoxPrompt', component: outBoxPrompt },
  { path: '/iconsCopy.html', name: 'iconsCopy', component: iconsCopy },
  { path: '/promptGuitarist.html', name: 'promptGuitarist', component: promptGuitarist },
  { path: '/EminorScaleHelper.html', name: 'EminorScaleHelper', component: EminorScaleHelper },
//place2
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
